# aaguilar-blaze-library

> 

[![NPM](https://img.shields.io/npm/v/aaguilar-blaze-library.svg)](https://www.npmjs.com/package/aaguilar-blaze-library) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save aaguilar-blaze-library
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'aaguilar-blaze-library'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [aaguilar2015](https://github.com/aaguilar2015)
