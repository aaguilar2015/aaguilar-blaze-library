import React, { Component } from 'react'

import ExampleComponent from 'aaguilar-blaze-library'

export default class App extends Component {
  render () {
    return (
      <div>
        <ExampleComponent text='Test Library' />
      </div>
    )
  }
}
